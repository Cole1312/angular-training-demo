import { Injectable } from '@angular/core';

@Injectable()
export class CalculationService {

  constructor() { }

  sum (first: number, second: number) {
    return first + second;
  }
}
