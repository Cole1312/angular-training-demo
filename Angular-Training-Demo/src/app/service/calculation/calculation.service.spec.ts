import { TestBed, inject } from '@angular/core/testing';

import { CalculationService } from './calculation.service';

describe('CalculationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CalculationService]
    });
  });

  it('should be created', inject([CalculationService], (service: CalculationService) => {
    expect(service).toBeTruthy();
  }));

  it('should return correct value for the sum function', inject([CalculationService], (service: CalculationService) => {
    const result = service.sum(1, 2);
    expect(result).toBe(3);
  }));
});
