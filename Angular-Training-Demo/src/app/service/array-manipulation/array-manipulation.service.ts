import { Injectable } from '@angular/core';

@Injectable()
export class ArrayManipulationService {

  numberArray: number[];

  constructor() {
    this.numberArray = [1, 2, 3, 4, 5];
  }

  addOne(item: number) {
    this.numberArray.push(item);
  }

  removeOne() {
    this.numberArray.pop();
  }
}
