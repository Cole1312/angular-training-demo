import { TestBed, inject } from '@angular/core/testing';

import { ArrayManipulationService } from './array-manipulation.service';

describe('ArrayManipulationService', () => {
  const service = new ArrayManipulationService();
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ArrayManipulationService]
    });
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  xit('should add one to the end when calling addOne()', () => {
    service.addOne(6);
    expect(service.numberArray).toEqual([1, 2, 3, 4, 5, 6]);
  });

  xit('should remove one from the end when calling removeOne()', () => {
    service.removeOne();
    expect(service.numberArray).toEqual([1, 2, 3, 4, 5]);
  });



  it('should remove one from the end when calling removeOne()', () => {
    const length = service.numberArray.length;
    const lastItem = service.numberArray[length - 1];
    service.removeOne();
    expect(service.numberArray.length).toEqual(length - 1);
    expect(service.numberArray[service.numberArray.length - 1]).not.toEqual(lastItem);
  });

  it('should add one to the end when calling addOne()', () => {
    const length = service.numberArray.length;
    const lastItem = service.numberArray[length - 1];
    service.addOne(9);
    expect(service.numberArray.length).toEqual(length + 1);
    expect(service.numberArray[service.numberArray.length - 1]).not.toEqual(lastItem);
    expect(service.numberArray[service.numberArray.length - 1]).toEqual(9);
  });


  // xit('should remove one from the end when calling removeOne', inject([ArrayManipulationService], (service: ArrayManipulationService) => {
  //   service.removeOne();
  //   expect(service.numberArray).toEqual([1, 2, 3, 4]);
  // }));
});
