import { Component } from '@angular/core';
import {CalculationService} from './service/calculation/calculation.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular-Training-Demo!';
  first: number;
  second: number;
  result: number;

  constructor(private calculationService: CalculationService) {
  }
  addNumbers() {
    this.result = this.calculationService.sum(this.first, this.second);
  }
}
